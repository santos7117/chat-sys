import firebase from "firebase/app";
import "firebase/auth";

export const auth = firebase
  .initializeApp({
    apiKey: "AIzaSyC8dYxNf_8Ud6RDwAIYMzWLC6ijLCSejAY",
    authDomain: "unichat-c9768.firebaseapp.com",
    projectId: "unichat-c9768",
    storageBucket: "unichat-c9768.appspot.com",
    messagingSenderId: "410570566990",
    appId: "1:410570566990:web:44c9e48eed4daacade821f",
  })
  .auth();
