# 3.0 CSS3

# 3.1 Selectors

1. Element Selectors
  eg :
   p {
    font-family: san-serif;
   }

2. Class Selectors
  eg :
    <h2 class="bold-headings">Bold Heading</h2>
    .bold-headings {
      font-family: san-serif;
    }

3. ID Selectors
  eg :
    <h2 id="bold-headings">Bold Heading</h2>
    #bold-headings {
      font-family: san-serif;
    }

---

# 3.2 Specificity

Priority order:

- inlinestyle > id > class > element

---

# 3.3 Pseudoselectors

**defines a special state of an element, class or id**

  eg:
    p: hover {
      font-family:san-serif;
    }

---

# 3.4 Advanced Selectors

1. General Selectors
  eg:
  for every anchor tags that follows h2
    h2 + a {
      font-family: san-serif;
    }

2. General Sibling Selectors
  eg:
  for every anchor tags that follow h2 with same parent
    h2 ~ a {
      font-family: san-serif;
    }

3. Child Selector
  eg:
  for every direct child list of unordered list
    ul > li {
      color: purple;
    }

4. Descendent Selector
  eg: 
    <ui>
      <li>
        <ol>
          <li>
  for every descendent list of list
    ul li {
      color: purple;
    }

---

# 3.5 Attribute Selectors


  eg:
  for all h2 with class "subtitle"
    h2[class=subtitle] {
      font-size: 20;
    }

  for all images under images/ folder
    img[src^="./images/"] {
      margin-left: 20;
    }
  
  for all classes with whitespace attributes
    h2[class~=article-subtitle] {
      font-size: 20;
    }

---

# 3.6 Layouts

  content ---> padding ---> border ---> margin

# 3.7 Flexbox

## 3.7.1 Components
  1. Container
  2. Items